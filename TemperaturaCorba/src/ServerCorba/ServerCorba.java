package ServerCorba;

import java.util.Properties;

import org.omg.CosNaming.*;
import org.omg.PortableServer.*;
import TemperaturaApp.*;

public class ServerCorba {

	public static void main(String[] args) {
		
		try {
			
			Properties props = System.getProperties();	
			props.setProperty("org.omg.CORBA.ORBInitialHost", "localhost");
			props.setProperty("org.omg.CORBA.ORBInitialPort", "1050");
			
			org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init(args, props);
			POA rootPoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
			rootPoa.the_POAManager().activate();
			TempImpl serverImpl = new TempImpl();
			
			org.omg.CORBA.Object ref = rootPoa.servant_to_reference(serverImpl);
            Temperatura mRef = TemperaturaHelper.narrow(ref);
 
            org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

			String name = "temperatura";
			NameComponent path[] = ncRef.to_name(name);
			ncRef.rebind(path, mRef);

			System.out.println("Servidor iniciado... esperando respuestas ");
			orb.run();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
