package ServerCorba;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import TemperaturaApp.TemperaturaPOA;
import database.ConectarMariaDB;

public class TempImpl extends TemperaturaPOA{
	

	@Override
	public String registrarTemperatura(String valorTemperatura) {
		String resultado = "";
		try {
			String sql = "insert into Temperatura(valorTemperatura)values(?)";
			ConectarMariaDB c = new ConectarMariaDB();
			Connection conn = c.conectarMariaDB();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, valorTemperatura);
			ps.execute();
			resultado = "Registrado exitosamente en la base de datos";
			System.out.println(resultado);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultado;
	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
		
	}

}
