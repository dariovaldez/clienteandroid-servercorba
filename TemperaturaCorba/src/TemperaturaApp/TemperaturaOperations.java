package TemperaturaApp;


/**
* TemperaturaApp/TemperaturaOperations.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from Temperatura.idl
* sábado 30 de noviembre de 2019 12:46:25 Hora de Ecuador
*/

public interface TemperaturaOperations 
{
  String registrarTemperatura (String valorTemperatura);
  void shutdown ();
} // interface TemperaturaOperations
