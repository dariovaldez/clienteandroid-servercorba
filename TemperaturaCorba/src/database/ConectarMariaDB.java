package database;

import java.sql.*;

public class ConectarMariaDB {
	
	Connection conectar = null;
	String database = "SensorTemperatura";
	String puertoMariaDB= "3306";
	String url = "jdbc:mysql://localhost:"+ puertoMariaDB +"/" +database;
	String user = "dario";
	String password = "1234";
	
	public Connection conectarMariaDB() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conectar = DriverManager.getConnection(url, user, password);
			System.out.println("Se conecto correctamente al servidor de MariaDB");
		} catch (ClassNotFoundException | SQLException ex) {
			// TODO: handle exception
			System.out.println("No se conecta");
		}
		return conectar;
	}
	
	public void desconectar() {
		try {
			conectar.close();
			System.out.println("Se desconecta de MariaDB");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}


}
