package ServerSocketAndroid;

import java.net.*;
import java.util.Properties;

import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import TemperaturaApp.*;

import java.io.*;

public class ServerSocketAndroid {
	
	private static Socket clienteSocket;
	private static ServerSocket serverSocket;
    private static int puerto = 5000;
    private static BufferedReader br;
    private static InputStreamReader isr;
    private static String temperatura = "";
    
    private static Temperatura tempImpl;
    

	public static void main(String[] args) {
		
		System.out.println("Server iniciado ....");
		
		try {
			while (true) {
				serverSocket = new ServerSocket(puerto);
				clienteSocket = serverSocket.accept();
				isr = new InputStreamReader(clienteSocket.getInputStream());
				br = new BufferedReader(isr);
				temperatura = br.readLine();		
				System.out.println("Valor recibido de cliente android: " + temperatura);		
				isr.close();
				br.close();
				clienteSocket.close();
				serverSocket.close();
				
				Properties props = System.getProperties();
				props.setProperty("org.omg.CORBA.ORBInitialHost", "localhost");
				props.setProperty("org.omg.CORBA.ORBInitialPort", "1050");			
				org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init(args, props);
				org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
				NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
				String name = "temperatura";
				tempImpl = TemperaturaHelper.narrow(ncRef.resolve_str(name));
				
				String resultado = tempImpl.registrarTemperatura(temperatura);
				System.out.println(resultado);
				
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

}
